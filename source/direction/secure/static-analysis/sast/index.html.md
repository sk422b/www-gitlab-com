---
layout: sec_direction
title: "Category Direction - Static Application Security Testing (SAST)"
description: "Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities."
canonical_path: "/direction/secure/static-analysis/sast/"
---

- TOC
{:toc}

## SAST

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Complete](/direction/maturity/) |
| Content Last Reviewed | `2024-01-26` |

<!-- Note: This document includes TODO comments. Publishing the updated content with TODOs remaining is a way to share the overall direction earlier and more iteratively, rather than waiting for all of the content requested by the new template to be ready to publish. -->

### Introduction and how you can help
This direction page describes GitLab's plans for the SAST category, which checks source code to find possible security vulnerabilities.

This page is maintained by the Product Manager for [Static Analysis](/handbook/product/categories/#static-analysis-group), [Connor Gilbert](/company/team/#connorgilbert).

Everyone can contribute to where GitLab SAST goes next, and we'd love to hear from you.
The best ways to participate in the conversation are to:
- Comment or contribute to existing [SAST issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3ASAST) in the public `gitlab-org/gitlab` issue tracker.
- If you don't see an issue that matches, file [a new issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20Proposal%20-%20basic).
    - Post a comment that says `@gitlab-bot label ~"group::static analysis" ~"Category:SAST"` so your issue lands in our triage workflow.
- If you're a GitLab customer, discuss your needs with your account team.

### Overview

[GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/) runs on merge requests and the default branch of your software projects so you can continuously monitor and improve the security of the code you write.
SAST jobs run in your CI/CD pipelines alongside existing builds, tests, and deployments, so it's easy for developers to interact with.

While SAST uses sophisticated techniques, we want it to be simple to understand and use day-to-day, especially by developers who may not have specific security expertise.
So, when you [enable GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/#configuration), it automatically detects the programming languages used in your project and runs the right security analyzers.

We want to give everyone the tools they need to write high-quality code, so [basic SAST scans](https://docs.gitlab.com/ee/user/application_security/sast/#summary-of-features-per-tier) are available in every GitLab tier.
However, organizations that use GitLab SAST in their security programs should use Ultimate.
Only GitLab Ultimate [includes](https://docs.gitlab.com/ee/user/application_security/sast/#summary-of-features-per-tier):
- Proprietary technology for suppressing false positive results and tracking vulnerabilities as they move through codebases.
- Integrating SAST results into the merge request workflow.
- Vulnerability Management, Security Policies, and other capabilities that help you enforce security requirements.

### Strategy and Themes
<!-- Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

We want to help developers write better code and worry less about common security mistakes.
SAST should help _prevent_ security vulnerabilities by helping developers easily _identify_ common security issues as code is being contributed and _mitigate_ proactively.
SAST should _integrate_ seamlessly into a developer’s workflow because security tools that are actively used are effective.

- _Prevent_ - find common security issues as code is being contributed and before it gets deployed to production.
- _Identify_ - continuously monitor source code for known or common issues.
- _Mitigate_ - make it easy to remediate identified issues, automatically if possible.
- _Integrate_ - integrate with the rest of the DevOps pipeline and [play nicely with other vendors](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others).

The importance of these goals is validated by GitLab's [DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/#security), which consistently finds that:
- Many organizations include security testing too late in the software development lifecycle.
- Developers are critical to remediating application security issues, but often don't have access to security tools.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
 -->

GitLab Static Analysis and Vulnerability Research teams are collaborating to address important opportunities to improve the customer experience with SAST.

We're prioritizing these themes for feature delivery:

1. **Result quality:** Security professionals and developers should be able to trust every result from GitLab SAST.
  - This is our most important initiative, because detection is the key task for any SAST product.
      - This work improves the user experience directly, but also _indirectly_ by reducing the pressure on other workflows, for example by reducing how often users must complete workflows like result triage and analysis.
  - Our work in this area includes two major areas:
      - _Detection engine:_ Improving the detection engines used to process and analyze code, including the development of a new [next-generation detection engine](https://gitlab.com/groups/gitlab-org/-/epics/3260).
      - _Detection rules:_ Adding, removing, and updating the detection rules that the detection engines use.
1. **"Day 1" experience and "Day 2" efficiency:** We believe that SAST can improve every codebase by supporting human reviewers, so we want to make SAST easy to enable ("Day 1") and operate going forward ("Day 2"). Concretely, this includes:
  - Consolidating coverage from multiple separate analyzers to [Semgrep-based analysis using GitLab-created rules](https://gitlab.com/groups/gitlab-org/-/epics/5245). This provides a simpler, more consistent operational experience; allows GitLab to directly manage rules; and makes scans run faster.

We're prioritizing these themes for future design and discovery efforts, following recent feature releases:

1. **Triage and remediation experience:** We value our users' time, and making it easier to _understand_ a potential vulnerability improves the chances it'll be resolved.
    - We recently added [SAST findings in the MR diff view](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#sast-results-in-mr-changes-view).
    - We are now looking for new opportunities to improve this experience, with an emphasis on smaller changes that have outsize impact.
1. **Shifting further left:** GitLab SAST already scans code as soon as it's pushed, before code reviews even begin. But, we are investing in [IDE integrations](https://gitlab.com/groups/gitlab-org/-/epics/9679) to offer scanning even earlier, including before code leaves developer machines.
    - We recently released two major iterations of IDE integration, adding [CI/CD pipeline-based security scan results to the VS Code IDE](https://gitlab.com/groups/gitlab-org/-/epics/9004).
    - We have completed initial technical discussions toward [real-time SAST scanning in the IDE](https://gitlab.com/groups/gitlab-org/-/epics/10283).


#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next 3 months, we are planning to work on:
- **Result quality:**
    - Continuing to [update the SAST detection rules](https://gitlab.com/groups/gitlab-org/-/epics/10907) that GitLab creates and maintains, focusing first on removing low-value rules to reduce false-positives before focusing on adding new rules to reduce false-negatives.
- **"Day 1" experience and "Day 2" efficiency:**
    - [Consolidating additional language-specific analyzers](https://gitlab.com/groups/gitlab-org/-/epics/5245) by delivering GitLab-managed rules in the Semgrep-based analyzer.

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

We are currently working on:
- **Result quality:**
    - Continuing to [update the SAST detection rules](https://gitlab.com/groups/gitlab-org/-/epics/10907) that GitLab creates and maintains, focusing first on removing low-value rules to reduce false-positives before focusing on adding new rules to reduce false-negatives.
- **"Day 1" experience and "Day 2" efficiency:**
    - Consolidating [NodeJS scanning](https://gitlab.com/gitlab-org/gitlab/-/issues/395487) into the Semgrep-based analyzer.
        - This change will be fully released in 17.0 as part of the analyzer consolidations released each year in the major (`.0`) GitLab release.
    - Documenting intended changes to analyze coverage in the upcoming major GitLab 17.0 release.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

Our recent work includes:

- [Integrating SAST results into the MR diff view](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#sast-results-in-mr-changes-view) (in the Changes tab). This project involved updating the Code Quality inline diff feature and adding SAST results to it.
- Significant [improvements to the default SAST ruleset](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#updated-sast-rules-to-reduce-false-positive-results), delivering substantially fewer false-positive and false-negative results in 16.8 and future releases. See [epic 10907](https://gitlab.com/groups/gitlab-org/-/epics/10907) for further progress.
- [Improvements](https://gitlab.com/groups/gitlab-org/-/epics/11395) to the [automatic resolution of findings from removed rules](https://docs.gitlab.com/ee/user/application_security/sast/#automatic-vulnerability-resolution). This makes the rollout of rule removals significantly clearer for users.
- [Improvements](https://gitlab.com/groups/gitlab-org/-/epics/10996) on the recently released [integration of CI/CD pipeline-based security scan results into the VS Code IDE](https://gitlab.com/groups/gitlab-org/-/epics/9004).
- [Improvements to Advanced Vulnerability Tracking (AVT)](https://gitlab.com/groups/gitlab-org/-/epics/5144). We expanded AVT to all possible analyzers and improved the algorithm to handle more cases. These improvements were released iteratively during [16.2](https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#improved-sast-vulnerability-tracking), [16.3](https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/#improved-sast-vulnerability-tracking), and [16.4](https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/#improved-sast-vulnerability-tracking).

Check [older release posts](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features&selectedCategories=SAST&minVersion=13_00) for our previous work in this area.

#### What is Not Planned Right Now

We understand the value of many potential improvements to GitLab SAST, but aren't currently planning to work on the following initiatives:

- **Expanding language support:** Currently, we're focusing on making it easier and faster to use SAST on [the many languages and frameworks we already support](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks), rather than adding support for new languages.
However, if we don't support a language you use, you can:
    - [Tntegrate third-party tools](https://docs.gitlab.com/ee/development/integrations/secure.html) (open-source or proprietary) using our [documented, open report format](https://docs.gitlab.com/ee/development/integrations/secure.html#report).
    - Document your request by opening an issue or commenting on an existing issue in [epic 297](https://gitlab.com/groups/gitlab-org/-/epics/297).
- **Incremental scanning:** We're currently focusing on making _all_ scans faster rather than developing different types of scans for different cases, such as [incremental scans](https://gitlab.com/gitlab-org/gitlab/-/issues/419734) that scan only modified code.

### What SAST is and isn't

SAST helps developers identify weaknesses and security issues early in the software development lifecycle, soon after code is written.

SAST _does_:

- Use static analysis techniques to find issues early in the development process.
- Identify weaknesses, which may be vulnerabilities, in the code it scans.
- Analyze the control and data flow of your program, check how functions are called, and otherwise analyze what the code could do at runtime.
- Help find issues that code reviewers or tests might miss.

SAST _doesn't_:

- Find known vulnerabilities in software dependencies; this is [software composition analysis](/direction/secure/composition-analysis/software-composition-analysis/).
- Run code and attempt to trigger unexpected behaviors; this is [dynamic analysis](/direction/secure/dynamic-analysis/).
- Look for generic bugs or maintenance issues; this is [Code Quality](/direction/secure/static-analysis/code_quality/).
- Replace code reviewers or tests; it augments them instead.

<!-- TODO: Add Best-in-Class Landscape section. -->
