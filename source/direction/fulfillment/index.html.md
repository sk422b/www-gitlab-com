---
layout: markdown_page
title: "Product Section Direction - Fulfillment"
description: "The Fulfillment section at GitLab focuses on supporting our customers to purchase, upgrade, downgrade, and renew paid subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2023-12

## Fulfillment Section Overview

At GitLab, Fulfillment works to provide a seamless buying experience for our customers. We invest in the [order-to-cash](https://handbook.gitlab.com/handbook/company/quote-to-cash/) systems to make purchasing, activating, and managing GitLab subscriptions as easy as possible. This improves customer satisfaction and streamlines our go-to-market (GTM) processes, helping accelerate revenue growth for the company.

The Fulfillment section encompasses [five groups](#groups) and [ten categories](https://handbook.gitlab.com/handbook/product/categories/). Fulfillment collaborates closely with [Field Operations](https://handbook.gitlab.com/handbook/sales/field-operations/), [Enterprise Applications](https://handbook.gitlab.com/handbook/business-technology/enterprise-applications/), [Billing Ops](https://handbook.gitlab.com/handbook/finance/accounting/finance-ops/billing-ops/), [Support](https://handbook.gitlab.com/handbook/support/readiness/operations/), and [Data](https://handbook.gitlab.com/handbook/business-technology/data-team/).

We welcome feedback on our initiatives. If you have any thoughts or suggestions, please feel free to create a merge request to this page and assign it to `@ofernandez2` for review, or [open a Fulfillment Meta issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/new).

### Mission

> Provide customers with an excellent experience by making it easy for them to purchase GitLab paid subscriptions and add-ons, provision their purchase, and manage subscription changes such as increasing seat count or renewing.

GitLab paid plans offer rich feature sets that enable customers to build software faster and more securely. For the Fulfillment section, success is to make it as easy as we can for a customer to transact with GitLab and unlock the value of these rich feature sets in our paid offerings.

We strive to make our subscription purchase and management process simple and support our customers' preferred purchasing channels, as well as their preferred payment methods. Delivering this vision requires investments across all interfaces where customers conduct business with GitLab. Given the breadth of countries, organization sizes, and industries that benefit from the GitLab product, we strive to be excellent at both direct transactions via our web commerce portal or our sales team, as well as sales via [Channels and Alliances](https://handbook.gitlab.com/handbook/sales/#channels--alliances).

### Impact on GitLab's addressable market

Our vision is to improve operational efficiency by providing a seamless end-to-end subscription management experience. This enables our Sales teams to spend more of their time on strategic accounts with [high LAM](https://handbook.gitlab.com/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#landed-addressable-market-lam). It also allows our Support and Finance teams to be more efficient.

## Recent Accomplishments

In FY23 and FY24, we took a deliberate approach to slow down new feature development in favor of improving the foundations of our systems and addressing pain points with existing functionality. This has continued in FY24 as we work to improve our system foundations. 

While improving our systems foundations, we have also delivered significant initiatives, including:

### Recent Customer Experience Improvements
1. [Allow customers that purchased via Reseller to access Customer Portal](https://gitlab.com/groups/gitlab-org/-/epics/8941/), this enabled customers that purchase their GitLab subscription via a reseller or partner to view their subscription details, provision their subscription, and manage their contact information without needing to open a support ticket. 
2. GitLab.com [namespace storage visibility](https://docs.gitlab.com/ee/user/usage_quotas.html#namespace-storage-limit)
3. Updates to [Cloud Licensing](https://docs.gitlab.com/ee/administration/license.html#activate-gitlab-ee) leading to improvements in activation rates and many customers benefiting from [increased efficiency managing licenses](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/#why-cloud-licensing).
4. Released [user caps for groups](https://docs.gitlab.com/ee/user/group/manage.html#user-cap-for-groups) to all namespaces, which helps gitlab.com customers better manage their paid subscription seats.
5. Automated licensing provisioning via Cloud Licensing for [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/).
6. Removed logic that was preventing some customers from activating their subscriptions [related Epic](https://gitlab.com/groups/gitlab-org/-/epics/9599).
1. Made it easier to access the [Customer Portal](https://customers.gitlab.com/) via GitLab.com SSO.

### Recent Pricing & Packaging Highlights
1. GitLab [Premium price change](https://about.gitlab.com/blog/2023/03/02/gitlab-premium-update/) including automated transitional pricing for existing customers.
2. [Enterprise Agile Planning add-on](https://about.gitlab.com/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/) for GitLab Ultimate subscriptions. 

## 1-year Plan

Our focus themes over the next 12 months continue to be: 

1. Enable pricing and packaging innovation at GitLab
2. Make it easier for customers to purchase from GitLab
3. Simplify our subscription model 
4. Streamline provisioning of subscriptions and trials
5. Improve subscription management
6. Drive internal team efficiency

### Enable pricing and packaging innovation at GitLab

GitLab has traditionally had limited offerings - 2 paid tier plans that apply to all seats in a provisioned instance/group, and 2 consumable add-ons. Moving forward, we'd like to enable other pricing and packaging options. Details of this work are [limited access](/handbook/communication/confidentiality-levels/#limited-access).

In addition, we support pricing changes such as the [GitLab Premium price change](/blog/2023/03/02/gitlab-premium-update/) announced on 2023-03-02.

### Make it easier for customers to purchase from GitLab

#### Enable channel partners and distributors to provide great selling motions

An increasing number of customers begin their GitLab journey via a partner. They may transact in a cloud provider's marketplace or purchase GitLab as part of a software bundle via a distributor. Our goal is to ensure those customers and partners get as high a quality of service as they would buying direct. This means extending our APIs to support indirect transactions. 

In FY24 Q4 we are scoping an integration with a reseller partner which will enable direct subscription orders and ammendments to happen without requiring any manual order processing. We plan to build this integration in FY25 and, based on its success and customer feedback, continue to expand on that investment. 

For more details on this work, reference the [Fulfillment Integrations category direction](/direction/fulfillment/fulfillment-integrations-and-visibility/).

#### Improve our self-service purchase experience

Currently, we maintain separate pathways for a new SaaS subscription purchase, a new self-managed subscription purchase, purchasing a storage add-on, and more. We also maintain purchase pathways for personal namespace add-ons, where we no longer support paid plan purchases. In FY25, we seek to simplify the purchase experiences by investing in a consolidated, best-in-class purchase path for our various offerings. Our investments in [using GitLab.com single sign-on as the login method for customers.gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/8905) open up the possibility for more streamlined purchase experiences across SaaS and SM without relying on building experiences within the GitLab product.

For more details on this work, reference the [Subscription Management](/direction/fulfillment/subscription-management/). 

### Simplify our subscription model 

We want to take the complexity out of understanding GitLab's [pricing](/company/pricing/) so that a customer can easily understand and manage their subscription usage and how it relates to billing. A key problem area has been seat overages transparency, [seat usage visibility](https://gitlab.com/groups/gitlab-org/-/epics/5872), and customer understanding of our overages model. We are looking for ways to simplify our seat overages model and hope to finalize a proposal by the end of FY24, for FY25 development. 

As we roll out [namespace Storage limits on gitlab.com](/pricing/faq-efficient-free-tier/#storage-limits-on-gitlab-saas-free-tier), we are working to ensure that [namespace storage is easy to understand](https://gitlab.com/groups/gitlab-org/-/epics/8852) and manage for all gitlab.com users.

### Streamline provisioning of subscriptions and trials

Our current focus areas include: 
1. Enabling gitlab.com Ultimate trials on namespaces with an active Premium plan.

In the future, we plan to also work on:
1. Allowing customers to manage and use a single subscription across multiple SM instances and SaaS namespaces.

For more details on this work, reference the [SaaS Provisioning](/direction/fulfillment/saas-provisioning/) and [Self-Managed Provisioning](/direction/fulfillment/sm-provisioning/) direction pages.

### Improve subscription management

Managing a GitLab subscription should be simple and largely automated. In order to make this a reality for all customers, we are investing in:
1. Making renewal emails more relevant, clear and actionable for the customers.
2. Revamp our [customer portal's subscription card](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/5317) to make it easy for subscription admins to understand their subscription and manage it. 
3. Add the ability for multiple users to manage the same subscription in our Customer Portal [Epic](https://gitlab.com/groups/gitlab-org/-/epics/10495).

For more details on this work, reference the [Subscription Management category direction](/direction/fulfillment/subscription-management/)

### Drive internal team efficiency

GitLab team members are passionate about delivering value to our customers. We are investing in the following initaitives to better enable them to do this: 

1. [Aligning customers.gitlab.com and Zuora billing account models](https://gitlab.com/groups/gitlab-org/-/epics/8331).
1. Making key internal system integrations more robust and resilient.
1. Investing in Fulfillment developer productivity via code refactors and new tooling deployment.
1. Making it easier for [customer-facing team members to resolve licensing issues around renewals](https://gitlab.com/groups/gitlab-org/-/epics/10173). 

As we complete these investments we will reduce the complexity of our order-to-cash systems, making it easier to innovate and deliver improvements to GitLab customers and our internal stakeholders across sales, billing, and more.

For more details on this work, reference the Fulfillment Platform groups direction pages and roadmaps in [Groups](#groups).

## Roadmap

Due to the [not public](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/#not-public) nature of most of our projects, our product roadmap is internal. 

We have [Fulfillment FY24 Plans and Prioritization](https://gitlab.com/gitlab-com/Product/-/issues/5291) (also Not Public), that GitLab team members can reference to track all planned initiatives by theme.

### Roadmap Prioritization

To learn more about our roadmap prioritization principles and process, please see [Fulfillment Roadmap Prioritization](/handbook/product/fulfillment-guide/#fulfillment-roadmap-prioritization)

## Groups


| Group | Description | Categories |
|------------|---------------|----------|-------|
| Subscription Management | The Subscription Management group is responsible for providing customers with an easy, informed, and reliable experience to view and manage their subscriptions, billing account details and contacts. | [Subscription Management](/direction/fulfillment/subscription-management/) |
| [Fulfillment Platform](/direction/fulfillment/platform/) | Fulfillment Platform maintains and evolves our underlying order-to-cash infrastructure, including integrations with Zuora to help accelerate our goals as a section. This group also works with internal teams to build robust systems that enable our internal, customer-facing teams better support our customers. | [CustomersDot Application](/direction/fulfillment/platform/customers-dot-application/), [Fulfillment Admin Tooling](/direction/fulfillment/platform/fulfillment-admin-tooling/), [Fulfillment Infrastructure](/direction/fulfillment/platform/fulfillment-infrastructure/) |
| [Provision](/direction/fulfillment/provision/) | The Provision group is responsible for provisioning and managing licenses across self-managed and SaaS (including Cloud License activation/sync and provisioning of legacy licenses). This group is also responsible for the buying experience for customers provisioning instances through third-party distributors and marketplaces, as well as per user seat provisioning for Add ons | [SM Provisioning](/direction/fulfillment/sm-provision/), [SaaS Provisioning](/direction/fulfillment/saas-provision/), [Fulfillment Integrations & Visibility](/direction/fulfillment/fulfillment-integrations-and-visibility/) |
| [Utilization](/direction/fulfillment/utilization/) | The Utilization group endeavors to capture and deliver usage data (currently focused on consumables) to internal team members, prospects, and customers so that they can make the best decision for their business needs. | `Consumables Cost Management` ,  `Seat Cost Management`  |

## OKRs

Team members can reference our [Fulfillment FY24 Q3 OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3241). 

We follow the [OKR (Objective and Key Results)](/company/okrs/) framework to set and track goals quarterly. The Fulfillment section OKRs are set across the entire [Quad](/handbook/product/product-processes/#pm-em-ux-and-set-quad-dris).

## Performance Indicators

Fulfillment does not track Performance Indicators at this time. While we monitor performance metrics to ensure the availability, security, and robustness of our systems, and keep to our SLOs, our goals and product plans are all tracked in OKRs. 

## Recent Accomplishments and Learnings

See [Fulfillment Recap issues](https://gitlab.com/gitlab-com/Product/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=Fulfillment%20Recap&first_page_size=20) for recaps of recent milestones and the associated accomplishments and learnings (internal only when needed).

## Key Links

1. [Fulfillment Direction](/direction/fulfillment): Outlines the Fulfillment vision and what we are working on next.
2. [Fulfillment Guide](/handbook/product/fulfillment-guide/): documentation around CustomersDot Admin tools and process documentation that is not part of the [core product documentation](https://docs.gitlab.com/).
3. [Dev - Fulfillment Sub Department](/handbook/engineering/development/fulfillment/): R&D team, priorities, prioritization processes, and more.
4. [Internal Handbook - Fulfillment](https://internal.gitlab.com/handbook/product/fulfillment/): documentation that can't be in the public handbook. Minimize this to only [Not Public](/handbook/communication/confidentiality-levels/#not-public) information, such as revenue-based KPIs or sensitive project documentation.
