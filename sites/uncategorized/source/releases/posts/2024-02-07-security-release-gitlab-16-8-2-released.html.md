---
title: "GitLab Security Release: 16.8.2, 16.7.5, 16.6.7"
categories: releases
author: Ottilia Westerlund
author_gitlab: ottilia_westerlund
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.8.2, 16.7.5, 16.6.7 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/02/07/security-release-gitlab-16-8-2-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.8.2, 16.7.5, 16.6.7 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Restrict group access token creation for custom roles](#restrict-group-access-token-creation-for-custom-roles) | Medium |
| [Project maintainers can bypass group's scan result policy `block_branch_modification` setting](#project-maintainers-can-bypass-groups-scan-result-policy-block_branch_modification-setting) | Medium |
| [ReDoS in CI/CD Pipeline Editor while verifying Pipeline syntax](#redos-in-ci/cd-pipeline-editor-while-verifying-pipeline-syntax) | Medium |
| [Resource exhaustion using GraphQL `vulnerabilitiesCountByDay`](#resource-exhaustion-using-graphql-vulnerabilitiescountbyday) | Medium |

### Restrict group access token creation for custom roles

An issue has been discovered in GitLab EE affecting all versions starting from 16.8 before 
16.8.2. When a user is assigned a custom role with manage_group_access_tokens permission, 
they may be able to create group access tokens with Owner privileges, which may lead to privilege escalation. 
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N`, 6.5). 
It is now mitigated in the latest release and is assigned [CVE-2024-1250](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1250).


This vulnerability has been discovered internally by GitLab team member [Rohit Shambhuni](https://gitlab.com/rshambhuni).



### Project maintainers can bypass group's scan result policy `block_branch_modification` setting

An issue has been discovered in GitLab EE affecting all versions from 16.4 prior to 16.6.7, 16.7 prior to 16.7.5, and 16.8 prior to 16.8.2 which allows a maintainer to change the name of a protected branch that bypasses the security policy added to block MR.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:H/A:H`, 6.7).
It is now mitigated in the latest release and is assigned [CVE-2023-6840](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6840).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS in CI/CD Pipeline Editor while verifying Pipeline syntax.

A denial of service vulnerability was identified in GitLab CE/EE, affecting all versions from 15.11 prior to 16.6.7, 16.7 prior to 16.7.5 and 16.8 prior to 16.8.2 which allows an attacker to spike the GitLab instance resource usage resulting in service degradation.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2023-6386](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6386).

Thanks `Anonymizer` for reporting this vulnerability through our HackerOne bug bounty program.


### Resource exhaustion using GraphQL `vulnerabilitiesCountByDay`

An issue has been discovered in GitLab EE affecting all versions from 13.3.0 prior to 16.6.7, 16.7 prior to 16.7.5, and 16.8 prior to 16.8.2 which allows an attacker to do a resource exhaustion using GraphQL `vulnerabilitiesCountByDay`.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-1066](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1066).

This vulnerability has been discovered internally by GitLab team member [Brian Williams](https://gitlab.com/bwill).


### Update to PostgreSQL 14.10 and 13.13

PostgreSQL has been updated.



## Non Security Patches

### 16.8.2

* [Gitaly: properly set PYTHON_TAG in CI, for Dockerfile (16.8)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1714)
* [Update GDK base build image and update QA GEM](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142960)
* [Revert "Validate scopes for importing collaborators"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142919)
* [Backport !142896 into 16.8 stable branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142926)
* [Update dependency prometheus-client-mmap to '~> 1.1', '>= 1.1.1'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143172)
* [Defer ConnectionPool instrumentation setup](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143173)
* [Add item_to_preload method in helper and migrations to prevent N+1 query](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143302)
* [Fix bug for devfile with multiple container components](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143316)
* [Backport "Fix Redis 6.0 compatibility breakage with Sidekiq 7 gem"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143526)
* [Finalize UUID backfilling before performing cleanup](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142906)
* [Backport - Ensure post upgrade steps are run after geo_pg_upgrade](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7402)

### 16.7.5

* [Update dependency prometheus-client-mmap to '~> 1.1', '>= 1.1.1'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143174)
* [Backport UUID migration finalization to 16.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143500)

### 16.6.7

* [Add missing IMAGE_TAG_EXT to referenced PostgreSQL image](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1708)
* [Backport: Update GDK base build image](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143579)
* [[Backport] Control runner tags for package promotion via env vars](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7388)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
